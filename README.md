# ChibiOS bootstrap project

This slight base aims to speed up the creation of a new ChibiOS project.

## Prep

Download ChibiOS

    make install

To download µGFX at the same time :

    make install UGFX=true

Then copy the following files from the demo corresponding to your board, or to
the board with the same MCU as yours :

- `Makefile` (rename to `Makefile.chibios`)
- `chconf.h`
- `halconf.h`
- `mcuconf.h`
- `main.c` (optional but provides a base file)

Don't forget to edit the following variables in `Makefile.chibios` :

- `PROJECT`: your project name
- `CHIBIOS`: change to `ChibiOS` if you used `make install`, else change to the
  path where you have downloaded ChibiOS
- `CSRC` : add your own `.c` files in here

Then, if you want to use the µGFX library, follow
[their guide](https://wiki.ugfx.io/index.php/Using_ChibiOS/RT) to edit
`Makefile.chibios` and add the library to your project.

To be able to debug with OpenOCD with `make openocd`, edit the following
variables in `Makefile` :

- `OCD_BOARD`: set to the board you are using (according to OOCD), or to a board
  that hosts the same MCU as you. List files in
  `/usr/share/openocd/scripts/board` to find the proper one.
- `SERIAL_DEVICE`: put the proper device here if your board does not get listed
  as the serial device `/dev/ttyACM0`

## Usage

A good thing is to leave OpenOCD running in the background, as long as the
board/JTAG programmer is plugged. Do so with `make openocd`.

To compile your code, simply run `make`.

To send the compiled code to the MCU, run `make debug`. Once GDB welcomes you
with its prompt, type `c` to *c*ontinue the execution.

To open a the MCU's serial port (if any), run `make serial`

## Semi-doc

Make targets :

- `install`: downloads/updates ChibiOS from the git repository
- `openocd`: starts OpenOCD GDB server
- `debug`: starts GDB, flashes the target and pauses execution
- `serial`: opens the serial port the device may expose
- `clean`: removes build files

